<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>简易计算器</title>
    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link
            href="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/css/bootstrap.min.css"
            rel="stylesheet">
    <!-- jQuery文件。务必在bootstrap.min.js 之前引入 -->
    <script src="https://cdn.staticfile.org/jquery/2.1.1/jquery.min.js"></script>
    <!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
    <script
            src="https://cdn.staticfile.org/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

</head>
<body>
<form>
    数字一：<input name="num1" type="text" value="2">
    <select name="sign">
        <option value="1">+</option>
        <option value="2">-</option>
        <option value="3">*</option>
        <option value="4">/</option>
    </select>
    数字二：<input name="num2" type="text" value="1">
    <button type="button">计算</button>
    结 果：<input id="result" name="result" type="text" value="0.0">
</form>

<script type="text/javascript">

    $(document).ready(function () {
        $("button").click(function () {
            $.ajax({
                url: "/caljson",
                contentType: "application/json",
                type: "POST",
                data: JSON.stringify($('form').serializeObject()),
                dataType: "json",
                success: function (data) {
                    // alert(200);
                    $("#result").val(data);
                }
            });
        });
    });

    //定义serializeObject方法，序列化表单
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        console.log(o);
        return o;
    }
</script>
</body>
</html>
