<%--
  Created by IntelliJ IDEA.
  User: DeCIDE
  Date: 2022/4/23
  Time: 14:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<form action="/calculate" method="post">
    <table border="1">
        <tr>
            <td><input type="text" id="num1" placeholder="请输入第一个数" onkeyup="value=value.replace(/[^\d]/g,'')"/></td>
            <td>
                <select id="operation">
                    <option value="+">+</option>
                    <option value="-">-</option>
                    <option value="*">*</option>
                    <option value="/">/</option>
                </select>
            </td>
            <td><input type="text" id="num2" placeholder="请输入第二个数" onkeyup="value=value.replace(/[^\d]/g,'')"/></td>
            <td><input type="button" value="=" onclick="calculate();"/></td>
            <td><input type="text" name="result" id="result"/>
                <!--<input th:text="${result}" />-->
            </td>
        </tr>
    </table>
    <input type="button" onclick="getResult()" value="获得结果"/>
</form>
</body>
<script type="text/javascript" src="https://cdn.staticfile.org/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript">

    /*function calculate() {
        var a = parseInt(document.getElementById("num1").value);
        var b = document.getElementById("operation").value;
        var c = parseInt(document.getElementById("num2").value);
        var result = '';
         switch (b) {
             case '+':
                 result = a + c;
                 alert(result);
                 break;
             case '-':
                 result = a - c;
                 alert(result);
                 break;
             case '*':
                 result = a * c;
                 alert(result);
                 break;
             case '/':
                 if (c != 0) {
                     result = a / c;
                     alert(result);
                 }else{
                     alert("除法运算分母不能为0！");
                 }
                 break;
         }
        //设置输入框的计算结果
        document.getElementById("result").value = result;
     }*/
    function getResult() {
        var a = parseInt(document.getElementById("num1").value);
        var b = document.getElementById("operation").value;
        var c = parseInt(document.getElementById("num2").value);

        console.log(b, c);

        if (b == '/' && c == 0) {
            document.getElementById("result").value = "除法运算分母不能为0！";
            return;
        }

        $.ajax({
            url: "/calculate",    //请求的url地址
            dataType: "json",   //返回格式为json
            async: true,//请求是否异步，默认为异步，这也是ajax重要特性
            data: {
                a: a,
                b: b,
                c: c
            },    //参数值
            type: "POST",   //请求方式
            success: function (req) {
                //请求成功时处理
                // console.log(req)
                // alert(req);
                document.getElementById("result").value = req;
            },
            error: function () {
                document.getElementById("result").value = "error";
                console.log("error");
            }
        });

    }

</script>

</html>
