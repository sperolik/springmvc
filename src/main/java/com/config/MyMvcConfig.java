package com.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyMvcConfig implements WebMvcConfigurer {

    // ctrl+o
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {

        // ctrl+alt+单击  多光标
//        System.out.println("Debug===>" + "200");
        registry.addViewController("/").setViewName("index.jsp");
        registry.addViewController("/index.html").setViewName("index");
        //访问ajax计算器   127.0.0.1:8080/ajaxCalculate
        registry.addViewController("/ajaxCalculate").setViewName("ajaxCalculate");

    }
}
