package com.bean;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class CalController {

	@RequestMapping(value = "/caljson", method= RequestMethod.POST)
	@ResponseBody
	public double calJson(@RequestBody Map<String, Object> map, Cal cal, Model model) {
		//字典是一个抽象类，而地图是一个接口 map
//		System.out.println(map.get("num1"));

		//获取 变量 类型
//		System.out.println("=============>"+map.get("num1").getClass());

		//赋值
		cal.setNum1(Double.parseDouble((String) map.get("num1")));
		cal.setNum2(Double.parseDouble((String) map.get("num2")));

		double result = cal.getResult((String) map.get("sign"));
		model.addAttribute("result", String.valueOf(result));
		return result;
	}

}
