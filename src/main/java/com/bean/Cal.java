package com.bean;

public class Cal {
    private double num1;
    private double num2;
    private double result;

    public void setNum1(double num1) {
        this.num1 = num1;
    }

    public void setNum2(double num2) {
        this.num2 = num2;
    }

    public double getResult(String sign) {
        if (sign.equals("1"))
            this.result = num1 + num2;
        else if (sign.equals("2"))
            this.result = num1 - num2;
        else if (sign.equals("3"))
            this.result = num1 * num2;
        else if (sign.equals("4"))
            this.result = num1 / num2;
        return result;
    }
}
