package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

//计算结果
@Controller
//@RequestMapping("/calculator")
public class Calculator {

    @RequestMapping("/calculator")
    @ResponseBody
    public String calculator() {
        return "OK";
    }
}
