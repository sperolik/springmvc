package com.pojo;

//ajax计算器
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pojo.Expression;

@Controller
public class CalculateController {

    //函数触发条件
    @PostMapping("/calculate")
    @ResponseBody
//    public Model calculate
    public String calculate(String a, Character b, String c/*, Model model*/) {

        int num1 = Integer.parseInt(a);
        char oper = b;
        int num2 = Integer.parseInt(c);
        Expression expression = new Expression(num1, oper, num2);

        Integer result = expression.getResult();

        return result.toString();
//        model.addAttribute("result",result);
//        System.out.println(a);
//        return model;
    }
}