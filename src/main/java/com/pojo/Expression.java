package com.pojo;

//ajax计算器

import java.io.Serializable;

public class Expression implements Serializable {

    private Integer numOne;
    private Character operation;
    private Integer numTwo;
    private Integer result;

    public Expression(int num1, char oper, int num2) {
        this.numOne = num1;
        this.operation = oper;
        this.numTwo = num2;
    }

    public Integer getNumOne() {
        return numOne;
    }

    public Integer getNumTwo() {
        return numTwo;
    }

    public Character getOperation() { return operation; }

    public Integer getResult() {
        switch (getOperation()) {
            case '+':
                this.result = getNumOne() + getNumTwo();
                break;
            case '-':
                this.result = getNumOne() - getNumTwo();
                break;
            case '*':
                this.result = getNumOne() * getNumTwo();
                break;
            case '/':
                try {
                    if (getNumTwo() != 0) {
                        this.result = getNumOne() / getNumTwo();
                    } else {
                        throw new RuntimeException("除法运算，分母不能为0！");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
        return result;
    }


}